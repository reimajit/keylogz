﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;


namespace Keylogger
{
    abstract class EncryptEngine
    {
        //Kryptionsnyckeln
        private const string ENCRYPTION_KEY = "Wx0Te1rc0pA";

        // Detta genererar "saltet" vilket stärker krypteringen.
        private readonly static byte[] SALT = Encoding.ASCII.GetBytes(ENCRYPTION_KEY);
        private readonly static byte[] key;
        private readonly static byte[] iv;
        private static readonly Rfc2898DeriveBytes keyGenerator;

        //Constructor;
        static EncryptEngine()
        {
            //Rfc2898DeriveBytes använder pseudo-random nummer generator baserat på HMACSHA1. 
            keyGenerator = new Rfc2898DeriveBytes(ENCRYPTION_KEY, SALT);
            key = keyGenerator.GetBytes(32);
            //Genererar "start-vektorn"
            iv = keyGenerator.GetBytes(16);
        }

        // Krypterar strängar med "Rijndael" alogaritmen.
        public static string Encrypt(string inputText)
        {
            //Skapar RijndaelManaged cipher för symmetrisk algorithm från nyckeln och start-vektorn
            RijndaelManaged rijndaelCipher = new RijndaelManaged { Key = key, IV = iv };

            byte[] plainText = Encoding.Unicode.GetBytes(inputText);

            using (ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor())
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainText, 0, plainText.Length);
                        cryptoStream.FlushFinalBlock();
                        return Convert.ToBase64String(memoryStream.ToArray());
                    }
                }
            }
        }
    }
}